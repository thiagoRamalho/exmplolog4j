package br.com.log;

import org.apache.log4j.Logger;

public class Exemplo {

	static Logger logger = Logger.getLogger(Exemplo.class);
	
	public Exemplo(){
		logger.debug("create...");

	}
	
	public void doSomeThing(){
		logger.debug("execute...");
	}

	public void finalize(){
		logger.debug("finalize...");
	}
}
