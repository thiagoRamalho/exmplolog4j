package br.com.log;

import org.apache.log4j.Logger;

public class ExemploLog {

	static Logger logger = Logger.getLogger(ExemploLog.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Exemplo exemplo = new Exemplo();		
		exemplo.doSomeThing();
		exemplo.finalize();
		
		logger.debug("teste");
		
	}

}
